class AddIndexToUsersEmail < ActiveRecord::Migration[5.0]
  def change
    #user Tableのemailカラムについては重複を防止してね
    add_index :users, :email, unique: true
  end
end
