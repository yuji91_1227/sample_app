class UsersController < ApplicationController
 
 #順番も重要
 before_action :logged_in_user, only: [:index, :edit, :update, :destroy, 
                                       :following, :followers]
 before_action :correct_user,   only: [:edit, :update]
 before_action :admin_user,     only: :destroy
 #[STUDY]
 #before_actionでredirect_toを使っているため、ログインしていない時は
 #後続の処理をスキップしてトップページに移動する
 
 
  # GET /users/:id
  # => app/views/users/show.html.erb
  def new
    @user = User.new
  end

  def show
    @user = User.find(params[:id])
    @microposts = @user.microposts.paginate(page: params[:page])
  end

  def index
    @users = User.paginate(page: params[:page])
  end

  def create
    @user = User.new(user_params)

    #[STUDY]
    #User.new(name: params[:user][:email])
    #User.new(name: params[:hash][:hash]...)
    #params[:user]
    #         | -- [:name ]
    #         | -- [:email]
    
    #多重代入、マスアサインメント --> 適切な慣習に沿っていないといけない
    #セキュリティ上の警告、Browserで例外としてActiveModel AttributeErrorが表示される

    if @user.save
      @user.send_activation_email
      flash[:info] = "Please check your email to activate your account."
      redirect_to root_url
    
    #Railsでは全て等価  
    #redirect_to user_path(@user.id)
    #redirect_to user_path(@user) --> defaultでidが呼び出される
    #redirect_to @user --> GET Request to '/user/id'
    
    else
      render 'new'
    end
  end
  
  def edit
    @user = User.find(params[:id])
  end
  
  # PATCH /users/:id
  def update
    @user = User.find(params[:id])
    if @user.update_attributes(user_params)
      flash[:success] = "Profile updated"
      redirect_to @user
    else
      render 'edit'
    end
  end

  def destroy
    User.find(params[:id]).destroy
    flash[:success] = "User deleted"
    redirect_to users_url
  end
  
  #Actionを追加
  def following
    @title = "Following"
    @user  = User.find(params[:id])
    @users = @user.following.paginate(page: params[:page])
    render 'show_follow'
  end

  def followers
    @title = "Followers"
    @user  = User.find(params[:id])
    @users = @user.followers.paginate(page: params[:page])
    render 'show_follow'
  end
  
  private
  
    #[STUDY] 
    #Strong Parameters
    #多重代入時にpermitメソッドを利用してセキュリティを担保する
    
    def user_params
      params.require(:user).permit(:name, 
                                   :email, 
                                   :password,
                                   :password_confirmation)
    end

    # beforeアクション

   # ログイン済みユーザーかどうか確認
   #app/controllers/application_controller.rbに移動  
   #def logged_in_user
   #  unless logged_in?
   #    store_location
   #    flash[:danger] = "Please log in."
   #    redirect_to login_url
   #  end
   #end

    # 正しいユーザーかどうか確認
    def correct_user
      @user = User.find(params[:id])
      redirect_to(root_url) unless current_user?(@user)
    end
    
    # 管理者かどうか確認
    def admin_user
      redirect_to(root_url) unless current_user.admin?
    end
end