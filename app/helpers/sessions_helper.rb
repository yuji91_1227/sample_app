module SessionsHelper
  # 渡されたユーザーでログインする
  def log_in(user)
    session[:user_id] = user.id
  end

  # 渡されたユーザーがログイン済みユーザーであればtrueを返す
  def current_user?(user)
    user == current_user
  end
  
  # ユーザーを永続的セッションに記憶する
  def remember(user)
    user.remember
    cookies.permanent.signed[:user_id] = user.id
    cookies.permanent[:remember_token] = user.remember_token
  end
  
    # 記憶トークンcookieに対応するユーザーを返す
  def current_user
    #ログインしている場合、セッションにアクセスせずにユーザー情報を返す
    return @current_user if @current_user
    # 現在ログイン中のユーザーを返す (いる場合)
    if (user_id = session[:user_id])
      @current_user ||= User.find_by(id: user_id)
    # 現在ログイン中のユーザーがいない場合、RememberTokenからUserを返す
    elsif (user_id = cookies.signed[:user_id])
      user = User.find_by(id: user_id)
      if user && user.authenticated?(:remember, cookies[:remember_token])
        log_in user
        @current_user = user
      end
    end
  end
  
  # 現在ログイン中のユーザーを返す (いる場合)
  #def current_user
  #  @current_user ||= User.find_by(id: session[:user_id])
  #end
  
 #def current_user
 #  if (user_id = session[:user_id])
 #    @current_user ||= User.find_by(id: user_id)
 #  elsif (user_id = cookies.signed[:user_id])
 #    user = User.find_by(id: user_id)
 #    if user && user.authenticated?(:remember, cookies[:remember_token])
 #      log_in user
 #      @current_user = user
 #    end
 #  end
 #end
  
  # ユーザーがログインしていればtrue、その他ならfalseを返す
  def logged_in?
    !current_user.nil?
  end
 
  # 現在のユーザーをログアウトする
  def log_out
    session.delete(:user_id)
    @current_user = nil
  end

  # 記憶したURL (もしくはデフォルト値) にリダイレクト
  def redirect_back_or(default)
    #sessionがあればそのURLに遷移。なければdefaultを返す
    redirect_to(session[:forwarding_url] || default)
    session.delete(:forwarding_url)
  end

  # アクセスしようとしたURLを覚えておく
  def store_location
    #GETリクエストの場合のみ
    session[:forwarding_url] = request.original_url if request.get?
  end
  
  # 永続的セッションを破棄する
  def forget(user)
    user.forget
    cookies.delete(:user_id)
    cookies.delete(:remember_token)
  end

  # 現在のユーザーをログアウトする
  def log_out
    forget(current_user)
    session.delete(:user_id)
    @current_user = nil
  end
end
