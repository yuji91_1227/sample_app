class Relationship < ApplicationRecord
  # Relationshipから見たUserModelとの関連性を作成する
  
  # belongs_toはシンボルのidをテーブルから探す
  # belongs_to :user => "#{tableName}_id"

  # follower_idを見に行って欲しいけど、followerテーブルを探して欲しくない
  # => Userテーブルを探して、follower_idを探して欲しい
  belongs_to :follower, class_name: "User"
  belongs_to :followed, class_name: "User"

# belongs_to,has_many メソッドを定義するためのメソッド
# attr_accessorと同様

  validates :follower_id, presence: true
  validates :followed_id, presence: true
end
